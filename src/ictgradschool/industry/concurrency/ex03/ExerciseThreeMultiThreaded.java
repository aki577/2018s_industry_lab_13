package ictgradschool.industry.concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded
    {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    private long m = 0;

    @Override
    protected double estimatePI(long numSamples)
        {
        // TODO Implement this.
        List<Thread> Threads = new ArrayList<Thread>();
        long n = 100;               //Threads num
        long newSample = numSamples / n;
        for (int j = 0; j < n; j++) {
            Thread t = new Thread(new Runnable()
                {
                @Override
                public void run()
                    {
                    ThreadLocalRandom tlr = ThreadLocalRandom.current();

                    long numInsideCircle = 0;

                    for (long i = 0; i < newSample; i++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }
                    }
                    total(numInsideCircle);
                    }
                });
            t.start();
            Threads.add(t);
        }
        try {
            for (Thread t : Threads) {
                t.join();
            }
        } catch (InterruptedException err) {
            err.getMessage();
        }
        double estimatedPi = 4.0 * (double) m / (double) numSamples;
        return  estimatedPi;
        }

    private synchronized void total(long i)
        {
        m = m + i;
        }

    /**
     * Program entry point.
     */
    public static void main(String[] args)
        {
        new ExerciseThreeMultiThreaded().start();
        }
    }
