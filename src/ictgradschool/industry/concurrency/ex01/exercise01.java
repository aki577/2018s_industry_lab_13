package ictgradschool.industry.concurrency.ex01;

public class exercise01
    {
    public void start()
        {
        Runnable myRunnable = new Runnable()
            {
            @Override
            public void run()
                {
                    for (int i = 0; i <= 1000000; i++) {
                        System.out.print(i);
                    }
                }
            };

        Thread myThread = new Thread(myRunnable);
        myThread.start();

        myThread.interrupt();
        }

    public static void main(String[] args)
        {
        new exercise01().start();
        }
    }
