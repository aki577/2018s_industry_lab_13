package ictgradschool.industry.concurrency.ex04;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class ConcurrentBankingApp
    {
    public static void main(String[] args) throws InterruptedException
        {

        final ArrayBlockingQueue<Transaction> abq = new ArrayBlockingQueue<>(10);

        List<Transaction> transactions = TransactionGenerator.readDataFile();

        BankAccount account = new BankAccount();

        Thread producer = new Thread(() -> {
        for (Transaction t : transactions) {
            try {
                abq.put(t);
            } catch (InterruptedException err) {
                System.err.println(Thread.currentThread().getName() + " was interrupted and is quitting gracefully.");
            }
        }
        });

        List<Thread> Threads = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            Threads.add(new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Transaction t = abq.take();
                    SerialBankingApp.doTransaction(t, account);
                } catch (InterruptedException err) {
                    // HACK: Why did it get stuck here without a break?
                    break;
                }
            }

            while (abq.size() > 0) {
                try {
                    Transaction t = abq.take();
                    SerialBankingApp.doTransaction(t, account);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            }));
        }
        producer.start();
        for (Thread t : Threads) {
            t.start();
        }

        producer.join();

        for (Thread t : Threads) {
            t.interrupt();
        }

        for (Thread t : Threads) {
            t.join();
        }

        System.out.println("Final balance: " + account.getFormattedBalance());
        }
    }
